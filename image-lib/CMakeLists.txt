cmake_minimum_required(VERSION 3.8)

project(Image-lib VERSION 1.0.0 LANGUAGES CXX)

# compile library
add_library(image-lib STATIC 

include/image-lib/Image.h
src/Image.cc

include/image-lib/Color.h
src/Color.cc

)

target_compile_features(image-lib PUBLIC cxx_std_17)

#Include own headers, expose headers for the others
target_include_directories(image-lib 
    INTERFACE include 
    PRIVATE include/image-lib
)