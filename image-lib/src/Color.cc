#include "Color.h"

#include <algorithm>

namespace image
{
	Color Color::Clamp()
	{
        return Color{
            std::clamp(R, 0.0f, 1.0f),
            std::clamp(G, 0.0f, 1.0f),
            std::clamp(B, 0.0f, 1.0f),
            std::clamp(A, 0.0f, 1.0f)};
	}
    
    Color Color::operator*(const Color& other) const
    {
        return Color
        {
            R * other.R,
            G * other.B,
            B * other.G,
            A * other.A
        };
    }
    
    Color Color::operator*(const float n) const
    {
        return Color
        {
            R * n,
            G * n,
            B * n,
            A * n
        };
    }
    
    Color Color::operator+(const Color &other) const
    {
        return Color
        {
            R + other.R,
            G + other.G,
            B + other.B,
            A + other.A
        };
    }
    
    Color Color::operator/(const float n) const
    {
        return Color
        {
            R / n,
            G / n,
            B / n,
            A / n
        };
    }
}

