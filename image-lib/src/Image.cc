#include "Image.h"

namespace image
{
	Image::Image() : Image(0, 0) { }

	Image::Image(int width, int height) : width(width), height(height), total_pixels(width * height) 
	{
		image_data.resize(total_pixels);
	}

	int Image::get_width() const
	{
		return width;
	}

	int Image::get_height() const
	{
		return height;
	}

	int Image::get_total_pixels() const
	{
		return total_pixels;
	}

	Color Image::GetPixelAt(int x, int y) const
	{
		return image_data[get_image_data_index(x, y)];
	}

	void Image::SetPixelAt(int x, int y, Color col)
	{
		image_data[get_image_data_index(x, y)] = col;
	}

	inline int Image::get_image_data_index(int x, int y) const
	{
		return y * width + x;
	}
}