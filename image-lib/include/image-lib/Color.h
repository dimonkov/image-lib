#ifndef IMAGE_COLOR_H_
#define IMAGE_COLOR_H_

namespace image
{
	struct Color
	{
		float R, G, B, A;

		Color Clamp();
        
        Color operator*(const Color& other) const;
        
        Color operator+(const Color& other) const;
        
        Color operator*(const float n) const;
        
        Color operator/(const float n) const;
	};
}
#endif // !IMAGE_COLOR_H_
