#ifndef IMAGE_IMAGE_H_
#define IMAGE_IMAGE_H_

#include <vector>

#include "Color.h"

namespace image
{
	class Image
	{
	public:

		Image();

		Image(int width, int height);

		int get_width() const;
		int get_height() const;
		int get_total_pixels() const;

		Color GetPixelAt(int x, int y) const;
		void SetPixelAt(int x, int y, Color col);

	private:
		int width, height, total_pixels;
		std::vector<Color> image_data;

		int inline get_image_data_index(int x, int y) const;
	};
}
#endif // !IMAGE_IMAGE_H_
