#include "BmpSerialization.h"

#include "Util.h"

namespace image::serialization::bmp
{
	void WriteAsBmpImage(std::ostream &out, image::Image &img)
	{
		WriteFileHeader(out, img);
		WriteInfoHeader(out, img);

		int height = img.get_height();
		int width = img.get_width();

		int row_pad = CalculateRowBytePadding(img);

		for (int y = height - 1; y >= 0; y--)
		{
			for (int x = 0; x < width; x++)
			{
				//Serialize pixels
				Color c = img.GetPixelAt(x, y);
				out << (unsigned char)(c.B * 255)
					<< (unsigned char)(c.G * 255)
					<< (unsigned char)(c.R * 255);
			}

			//Serialize row padding
			for (int i = 0; i < row_pad; i++)
				out << (char)0x0;
		}
	}
	void WriteFileHeader(std::ostream &out, image::Image &img)
	{
		//Letters "BM"
		static const uint16_t bfType = 0x4D42;
		//File size
		uint32_t bfSize = CalculateFileSize(img);

		//Two reserved uint16_t's need to  be 0 by the standard
		static const uint16_t bfReserved1 = 0x0;
		static const uint16_t bfReserved2 = 0x0;

		//With these header sizes, offset will always be 54
		static const uint32_t bfOffBits = 54;

		WriteLittleEndian(out, bfType);
		WriteLittleEndian(out, bfSize);
		WriteLittleEndian(out, bfReserved1);
		WriteLittleEndian(out, bfReserved2);
		WriteLittleEndian(out, bfOffBits);
	}
	void WriteInfoHeader(std::ostream &out, image::Image &img)
	{
		int32_t biWidth = img.get_width();
		int32_t biHeight = img.get_height();

		static const uint16_t biPlanes = 1;
		//8 bits per channel * 3 channels = 24 bits per pixel
		static const uint16_t biBitCount = 24;

		//No compression for now
		static const uint32_t biCompression = 0;

		uint32_t biSizeImage = biWidth * biHeight;

		//Multiplication overflow, it's ok to set it to 0 according to the standard
		if (biHeight != 0 && biSizeImage / biHeight != biWidth)
		{
			biSizeImage = 0;
		}

		//No DPI metadata
		static const int32_t biXPelsPerMeter = 0;
		static const int32_t biYPelsPerMeter = 0;

		//Not using color pallete
		static const uint32_t biClrUsed = 0;
		static const uint32_t biClrImportant = 0;

		WriteLittleEndian(out, INFO_HEADER_SIZE);
		WriteLittleEndian(out, biWidth);
		WriteLittleEndian(out, biHeight);
		WriteLittleEndian(out, biPlanes);
		WriteLittleEndian(out, biBitCount);
		WriteLittleEndian(out, biCompression);
		WriteLittleEndian(out, biSizeImage);
		WriteLittleEndian(out, biXPelsPerMeter);
		WriteLittleEndian(out, biYPelsPerMeter);
		WriteLittleEndian(out, biClrUsed);
		WriteLittleEndian(out, biClrImportant);
	}
	int CalculateRowBytePadding(image::Image &img)
	{
		int i_width = img.get_width();

		//Rows are aligned to 4 bytes
		int rowSizeInBytes = i_width * 3 * sizeof(char);
		return rowSizeInBytes % 4 == 0 ? 0 : (4 - rowSizeInBytes % 4);
	}
	int CalculateFileSize(image::Image &img)
	{
		int i_width = img.get_width();
		int i_height = img.get_height();

		//3 bytes per color
		int rowSizeInBytes = i_width * 3 * sizeof(char);
		int rowRounded = rowSizeInBytes + CalculateRowBytePadding(img);
		return rowRounded * i_height + FILE_HEADER_SIZE + INFO_HEADER_SIZE;
	}
}