#ifndef IMAGE_BMP_SERIALIZATION_H_
#define IMAGE_BMP_SERIALIZATION_H_

#include <ostream>

#include "image-lib/Image.h"

namespace image::serialization::bmp
{
	const int FILE_HEADER_SIZE = 14;

	const int INFO_HEADER_SIZE = 40;

	void WriteAsBmpImage(std::ostream& out, image::Image& img);

	void WriteFileHeader(std::ostream& out, image::Image& img);

	void WriteInfoHeader(std::ostream& out, image::Image& img);

	int CalculateRowBytePadding(image::Image& img);

	int CalculateFileSize(image::Image& img);
}
#endif // !IMAGE_BMP_SERIALIZATION_H_
