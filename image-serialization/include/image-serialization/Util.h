#ifndef IMAGE_UTIL_H_  
#define IMAGE_UTIL_H_

#include <ostream>
#include <stdint.h>

namespace image
{
	namespace serialization
	{
#pragma region LittleEndian

		inline void WriteLittleEndian(std::ostream & out, int64_t value)
		{
			out << (char)(value & 0xFF)
				<< (char)((value & 0xFF00L) >> 8)
				<< (char)((value & 0xFF0000L) >> 16)
				<< (char)((value & 0xFF000000L) >> 24)
				<< (char)((value & 0xFF00000000L) >> 32)
				<< (char)((value & 0xFF0000000000L) >> 40)
				<< (char)((value & 0xFF000000000000L) >> 48)
				<< (char)((value & 0xFF00000000000000L) >> 56);
		}
		inline void WriteLittleEndian(std::ostream & out, int32_t value)
		{
			out << (char)(value & 0xFF)
				<< (char)((value & 0xFF00) >> 8)
				<< (char)((value & 0xFF0000) >> 16)
				<< (char)((value & 0xFF000000) >> 24);
		}
		inline void WriteLittleEndian(std::ostream & out, int16_t value)
		{
			out << (char)(value & 0xFF)
				<< (char)((value & 0xFF00) >> 8);
		}


		inline void WriteLittleEndian(std::ostream & out, uint64_t value)
		{
			WriteLittleEndian(out, (int64_t)value);
		}
		inline void WriteLittleEndian(std::ostream & out, uint32_t value)
		{
			WriteLittleEndian(out, (int32_t)value);
		}
		inline void WriteLittleEndian(std::ostream & out, uint16_t value)
		{
			WriteLittleEndian(out, (int16_t)value);
		}

#pragma endregion
#pragma region BigEndian
		inline void WriteBigEndian(std::ostream & out, int64_t value)
		{
			out << (char)((value & 0xFF00000000000000L) >> 56)
				<< (char)((value & 0xFF000000000000L) >> 48)
				<< (char)((value & 0xFF0000000000L) >> 40)
				<< (char)((value & 0xFF00000000L) >> 32)
				<< (char)((value & 0xFF000000L) >> 24)
				<< (char)((value & 0xFF0000L) >> 16)
				<< (char)((value & 0xFF00L) >> 8)
				<< (char)((value & 0xFFL));
		}
		inline void WriteBigEndian(std::ostream & out, int32_t value)
		{
			out << (char)((value & 0xFF000000L) >> 24)
				<< (char)((value & 0xFF0000L) >> 16)
				<< (char)((value & 0xFF00L) >> 8)
				<< (char)((value & 0xFFL));
		}
		inline void WriteBigEndian(std::ostream & out, int16_t value)
		{
			out << (char)((value & 0xFF00L) >> 8)
				<< (char)((value & 0xFFL));
		}


		inline void WriteBigEndian(std::ostream & out, uint64_t value)
		{
			WriteBigEndian(out, (int64_t)value);
		}
		inline void WriteBigEndian(std::ostream & out, uint32_t value)
		{
			WriteBigEndian(out, (int32_t)value);
		}
		inline void WriteBigEndian(std::ostream & out, uint16_t value)
		{
			WriteBigEndian(out, (int16_t)value);
		}
#pragma endregion
	}
}
#endif // !IMAGE_UTIL_H_
